/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package qualityFeatures.ZouNew;

import java.util.ArrayList;
import mslib.peak.Peak;
import mslib.spectrum.MassSpectrum;

/**
 * avg_rel_peak_intens: Average of relative peak intensity Relative Intensity of
 * precursor in parent spectrum : We defined the relative intensity of each peak
 * as the peak’s intensity divided by the intensity of the highest peak. When
 * specified, we only considered peaks with a relative intensity above a certain
 * threshold;we used 0.1.
 *
 * Source : Kristian Flikka, LennartMartens, Joël Vandekerckhove, Kris Gevaert,
 * and Ingvar Eidhammer , Improving the reliability and throughput of mass
 * spectrometry-basedproteomics by spectrumquality filtering , Proteiomcs, 2006,
 * DOI: 10.1002/pmic.200500309
 *
 * @author Aybuge
 */
public class AverageOfRelativePeakIntensity extends NumberofPeaksWithRelativeIntensityMoreThan01 {
    ArrayList<Peak> spectrumList = new ArrayList<Peak>();

    public AverageOfRelativePeakIntensity(String name, MassSpectrum ms, ArrayList<Peak> spectrumList) {
        super(name, ms, spectrumList);
    }

    @Override
    public double calcScore() {
        double sumOfRelativeIntensity = 0.0;
        ms.sort(Peak.byYdesc);
        Peak highlyIntensePeak = ms.getPeak(0);
        for (Peak p : spectrumList) {
            double relativeIntensity = p.getY() / highlyIntensePeak.getY();
            if (relativeIntensity > 0.1) {
                sumOfRelativeIntensity += relativeIntensity;
            }
        }
        return (double)(sumOfRelativeIntensity / super.calcScore());
    }
}
