/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package qualityFeatures.ZouNew;

import java.util.ArrayList;
import java.util.HashMap;
import mslib.peak.Peak;
import mslib.spectrum.MassSpectrum;
import seqs.AminoAcid;
import seqs.AminoAcids;

/**
 * F7 measures the presence of peak pairs of one doubly charged and the other
 * singly charged ions corresponding to an amino acid mass difference in the
 * spectrum S Source : Zou, An-Min Wu, Fang-Xiang Ding, Jia-Rui Poirier, Guy G,
 * Quality assessment of tandem mass spectra using support vector machine (SVM),
 * BMC bioinformatics, 2009, DOI : 10.1186/1471-2105-10-S1-S49
 *
 * @author aybuge
 */
public class PeakPairsFromOneSingleOneDouble extends Features {

    
    public PeakPairsFromOneSingleOneDouble(String name, MassSpectrum ms) {
        super(name, ms);
        
    }

    public double calcScore() {
        AminoAcids aas = new AminoAcids();
        aas.removeAA('I');
        aas.removeAA('Q');
        aas.removeAA('M');
        int countSingleDouble = 0;
        
        DetermineChargeOfSpectrum defineCharge = new DetermineChargeOfSpectrum(ms.getSpectrum());
        defineCharge.determineChargeOfSpectrum();
        ArrayList<Peak> returnDoublyCharged = DetermineChargeOfSpectrum.returnDoublyCharged();
        ArrayList<Peak> returnSinglyCharged = DetermineChargeOfSpectrum.returnSinglyCharged();
        int sizeOfDoublyChargedArray = returnDoublyCharged.size();
        int sizeOfSinglyChargedArray = returnSinglyCharged.size();
        
        
        for (AminoAcid a : aas.aaList){ 
        
        for (int k = 0; k < sizeOfDoublyChargedArray - 1; k++) {
            for (int i = 0; i < sizeOfSinglyChargedArray-1; i++) {
                double diff1 = returnDoublyCharged.get(k).getX() - ((returnSinglyCharged.get(i).getX() + MassOfH) / 2);
//                if (a.getMass() == diff1 || a.getMass() == (diff1 + 2) || a.getMass() == (diff1 - 2)) {
//                    countSingleDouble++;
//                }
                  if (Math.abs(diff1-a.getMass())<=2)
                      countSingleDouble++;
            }
        }
        }
        return countSingleDouble;
    }
}

/**
 * 
 * 
To develop the remaining 12 features, four variables for a given peptide mass spectrum S are defined as
dif1(m(x), m(y)) = m(x) - m(y)(1)
 

dif2(m(x),m(y))=m(x)−[(m(y)+m(H))/2]
sum1(m(x), m(y)) = m(x) + m(y)
sum2(m(x),m(y))=m(x)+[(m(y)+m(H)/2]
where m(x) and m(y) denote the m/z-values of peaks x and y in the spectrum S, respectively; m(H) is the mass of a hydrogen atom. A weighting factor is defined as

W(x,y)=(Ir(x)+Ir(y)) /2
where Ir(x) and Iy(x) represent the relative intensities of peaks x and y in the spectrum S, respectively.

* F5 - F7: Amino acid distances. These features measure how likely two peaks in a spectrum S differ by one of the
twenty amino acids. Define 
* 
* F5 = ∑{W(x, y)|dif1(m(x), m(y)) ≈  Mi, i = 1, 2,...,17}
* F6 = ∑{W(x, y)|dif1(m(x), m(y)) ≈  Mi, i = 1, 2,...,17}
* F7 = ∑{W(x, y)|dif2(m(x), m(y)) ≈  Mi/2, i = 1, 2,...,17} (8)
where Mi(i = 1, 2,?,17) are the 17 different masses of all 20 amino acids. This study considers all Methionine
amino acids to be sulfoxidized and does not distinguish three pairs of amino acids in their masses: Isoleucine vs. Leucine, Glutamine vs. Lysine, and sulfoxidized Methionine vs. Phenylalanine since the masses of each pair are very close.
The comparison implied by ? employs a tolerance, which was set to ± 0.5 Da for fragment ions and ± 2Da for parent mass in this paper. The feature F5 measures the presence of peak pairs of singly charged ions corresponding to an amino acid mass difference in the spectrum S; 
the feature F6 measures the presence of peak pairs of doubly charged ions corresponding to an amino acid mass difference in the spectrum S, and 
the feature F7 measures the presence of peak pairs of one doubly charged and the other singly charged ions corresponding to an amino acid mass difference in the spectrum S. 
The use of the weighting factors in the features is to account the increased likelihood of more intense peaks being true fragment ions.

 */