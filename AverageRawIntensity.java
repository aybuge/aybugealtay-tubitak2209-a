/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package qualityFeatures.ZouNew;

import java.util.ArrayList;
import mslib.peak.Peak;
import mslib.spectrum.MassSpectrum;

/**
 *F2: The average raw intensity of the peaks in the spectrum, log-transformed.
 * Source : Zou, An-Min Wu, Fang-Xiang Ding, Jia-Rui Poirier, Guy G, Quality assessment of tandem mass spectra using support vector machine (SVM), BMC bioinformatics, 2009, DOI : 10.1186/1471-2105-10-S1-S49
 * @author aybuge
 */
public class AverageRawIntensity extends NumOfPeaks {                           //F2: The average raw intensity of the peaks in the spectrum, log-transformed.

    public AverageRawIntensity(String name, MassSpectrum ms,ArrayList<Peak> spectrumList) {
        super(name, ms, spectrumList);
    }
    
    @Override
    public double calcScore() {
        double TIC = ms.getTIC();
        double averageIntensity =  (double)(TIC / spectrumSize);
        return (double) Math.log(averageIntensity);
    }
}
