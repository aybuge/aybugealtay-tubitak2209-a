/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.

Complements. These features measure how likely an N-terminus ion and a C-terminus ion in the spectrum
S are produced as the peptide fragments at the same pep- tide bond.
F10 : measures the presence of complementary peak pairs of one doubly charged and the other singly charged ions in the spectrum S.
F10 = ∑ {W(x, y)|sum2(m(x), m(y))≈ Mp/2 + 2m(H)}
sum2= ((m(x), m(y)) = m(x)+((m(y)+m(H))/2)
The comparison implied by ≈ employs a tolerance, which was set to ± 0.5 Da for fragment ions and ± 2
Da for parent mass in this paper.

Source : Zou, An-Min Wu, Fang-Xiang Ding, Jia-Rui Poirier, Guy G, 
Quality assessment of tandem mass spectra using support vector machine (SVM), BMC bioinformatics, 2009, DOI : 10.1186/1471-2105-10-S1-S49


 */
package qualityFeatures.ZouNew;

import java.util.ArrayList;
import mslib.peak.Peak;
import mslib.spectrum.MassSpectrum;
import static qualityFeatures.ZouNew.Features.MassOfH;
import seqs.Mass;

/**
 *
 * @author aybuge
 */
public class ComplementsFromOneSingleOneDouble extends Features{
    
    public ComplementsFromOneSingleOneDouble(String name, MassSpectrum ms) {
        super(name, ms);
    }

  

    public double calcScore() {
        DetermineChargeOfSpectrum defineCharge = new DetermineChargeOfSpectrum(ms.getSpectrum());
        defineCharge.determineChargeOfSpectrum();
        ArrayList<Peak> returnSinglyCharged = DetermineChargeOfSpectrum.returnSinglyCharged();
        ArrayList<Peak> returnDoublyCharged = DetermineChargeOfSpectrum.returnDoublyCharged();
        int sizeOfSinglyChargedArray = defineCharge.returnSinglyCharged().size();
        int sizeOfDoublyChargedArray = defineCharge.returnDoublyCharged().size();
        
        double counter = 0.0;
        for (int k = 0; k < sizeOfSinglyChargedArray - 1; k++) {
            for (int i = 0; i < sizeOfDoublyChargedArray -1; i++) {
                double sum = returnSinglyCharged.get(k).getX() + Mass.H + ((returnDoublyCharged.get(i).getX() + Mass.H) /2 );
//                    if (sum == (ms.getPrecursorMass() /2) + 2* MassOfH || sum==(ms.getPrecursorMass() /2) + 2* MassOfH +0.5 || sum == (ms.getPrecursorMass() /2) + 2* MassOfH - 0.5) {
//                    counter++;
                    
                    if (Math.abs(sum - (ms.getPrecursorMass()/2)) <= 2+ 2*MassOfH)
                    counter ++;                
            }
        }
        return counter;
    }
}

