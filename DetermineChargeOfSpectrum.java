/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package qualityFeatures.ZouNew;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import mslib.peak.Peak;
import mslib.spectrum.MassSpectrum;
import static qualityFeatures.ZouNew.Features.MassOfH;

/**
 *
 * @author aybuge
 * 
 * this class used toı determine charge of the spectrum. According to this, if the first quartile of the spectrum con
 */
public class DetermineChargeOfSpectrum  {                                     // Defines and returns the dobly and singly charged peak ArrayList

    public ArrayList<Peak> spectrumList = new ArrayList<Peak>();
    public int spectrumSize = 0;

    public DetermineChargeOfSpectrum( ArrayList<Peak> spectrumList) {
        
        this.spectrumList = spectrumList;
	this.spectrumSize = spectrumList.size();

    }
    public static ArrayList<Peak> doublyChargedPeaks = new ArrayList<Peak>();        
    public static ArrayList<Peak> singlyChargedPeaks = new ArrayList<Peak>();

    public static ArrayList<Peak> getDoublyChargedPeaks() {
        return doublyChargedPeaks;
    }

    public static void setDoublyChargedPeaks(ArrayList<Peak> doublyChargedPeaks) {
        DetermineChargeOfSpectrum.doublyChargedPeaks = doublyChargedPeaks;
    }

    public static ArrayList<Peak> getSinglyChargedPeaks() {
        return singlyChargedPeaks;
    }

    public static void setSinglyChargedPeaks(ArrayList<Peak> singlyChargedPeaks) {
        DetermineChargeOfSpectrum.singlyChargedPeaks = singlyChargedPeaks;
    }
    
    
    public void determineChargeOfSpectrum() {
        ArrayList<Peak> spectrumListCopy = new ArrayList<Peak>();                    // creates copy of existing arraylist for sorting and sorts the copyArrayList in descending order
        //System.arraycopy(spectrumList, 0, spectrumListCopy, 0, spectrumSize);
	spectrumListCopy.addAll(spectrumList);
        Collections.sort(spectrumListCopy, new Comparator<Peak>() {
            @Override
            public int compare(Peak p1, Peak p2) {
                return (int) (p2.getX() - p1.getX());

            }
        });
//        MassSpectrum ms = new MassSpectrum();
//        ms.setSpectrum(spectrumListCopy);
//        ms.sort(Peak.byXasc);
        
        int quartileBorder =(int)Math.ceil(spectrumList.size() * 0.25);                   // defines the quartile border. Because the ArrayList is sorted as descending order, first 25% is the best ones

        ArrayList<Double> doublyCharges = new ArrayList<Double>();              // adds the ms/ms values of maximum quartile's  to new arrayList
        for (int i = 0; i < quartileBorder; i++) {
            doublyCharges.add(spectrumListCopy.get(i).getX());
        }

        double errorrate = 0.0;
        for (int i = 0; i < spectrumSize; i++) {
            if (doublyCharges.contains(spectrumList.get(i).getX() + errorrate) || doublyCharges.contains(spectrumList.get(i).getX()) || doublyCharges.contains(spectrumList.get(i).getX() - errorrate)) {
                doublyChargedPeaks.add(spectrumList.get(i));
             
            } else {
                singlyChargedPeaks.add(spectrumList.get(i));
            
            }
        }
    }

    public static ArrayList<Peak> returnDoublyCharged() {
        return doublyChargedPeaks;
    }
     public static ArrayList<Peak> returnSinglyCharged() {
        return singlyChargedPeaks;
    }
}
