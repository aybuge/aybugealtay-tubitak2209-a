/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package qualityFeatures.ZouNew;

import java.util.ArrayList;
import mslib.peak.Peak;
import mslib.spectrum.MassSpectrum;

/**
 * raw_total_intesity_threshold : Total raw intensities for significant peaks (significant refers to the ones that have relative intensity > 0.1)
 * 
 * Source : Kristian Flikka, LennartMartens, Joël Vandekerckhove, Kris Gevaert,
 * and Ingvar Eidhammer , Improving the reliability and throughput of mass
 * spectrometry-basedproteomics by spectrumquality filtering , Proteiomcs, 2006,
 * DOI: 10.1002/pmic.200500309
 * 
 * @author Aybuge
 */
public class TotalRawIntensitiesOfSignificantPeaks extends Features{
    ArrayList<Peak> spectrumList = new ArrayList<Peak>();
    public TotalRawIntensitiesOfSignificantPeaks(String name, MassSpectrum ms, ArrayList<Peak> spectrumList) {
        super(name, ms);
        this.spectrumList = spectrumList;
    }
    
    @Override
    public double calcScore(){
        ms.sort(Peak.byYdesc);
        Peak highlyIntensePeak = ms.getPeak(0);
        double totalIntensity = 0.0;
        for(Peak p : spectrumList){
            if(p.getY()/highlyIntensePeak.getY() > 0.1)
                totalIntensity += p.getY();
        }            
    return totalIntensity;    
    }
    
}
