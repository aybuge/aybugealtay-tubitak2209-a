/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package qualityFeatures.ZouNew;

import java.util.ArrayList;
import mslib.peak.Peak;
import mslib.spectrum.MassSpectrum;

/**
 * avg_peak_density : Number of peaks / (max_mz - min_mz)
 *
 * Source : Kristian Flikka, LennartMartens, Joël Vandekerckhove, Kris Gevaert,
 * and Ingvar Eidhammer , Improving the reliability and throughput of mass
 * spectrometry-basedproteomics by spectrumquality filtering , Proteiomcs, 2006,
 * DOI: 10.1002/pmic.200500309
 *
 * @author Aybuge
 */
public class AveragePeakDensity extends NumOfPeaks {

    public AveragePeakDensity(String name, MassSpectrum ms, ArrayList<Peak> spectrumList) {
        super(name, ms, spectrumList);
      
    }

    @Override
    public double calcScore() {
        double max_mz = ms.getHighMZ();
        double min_mz = ms.getLowMZ();
        return (super.calcScore() / (max_mz - min_mz));
    }
}
