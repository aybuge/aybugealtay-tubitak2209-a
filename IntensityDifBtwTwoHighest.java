/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package qualityFeatures.ZouNew;

import java.util.ArrayList;
import mslib.peak.Peak;
import mslib.spectrum.MassSpectrum;

/**
 * delta_two_highest : Intensity difference btw top two peaks Source : Kristian
 * Flikka, LennartMartens, Joël Vandekerckhove, Kris Gevaert, and Ingvar
 * Eidhammer , Improving the reliability and throughput of mass
 * spectrometry-basedproteomics by spectrumquality filtering , Proteiomcs, 2006,
 * DOI: 10.1002/pmic.200500309
 *
 * @author Aybuge
 */
public class IntensityDifBtwTwoHighest extends Features {

    public IntensityDifBtwTwoHighest(String name, MassSpectrum ms) {
        super(name, ms);

    }

    @Override
    public double calcScore() {
        ms.sort(Peak.byYdesc);
        Peak highlyIntensePeak1 = ms.getPeak(0);
        Peak highlyIntensePeak2 = ms.getPeak(1);
        return highlyIntensePeak1.getY() - highlyIntensePeak2.getY();
    }

}
