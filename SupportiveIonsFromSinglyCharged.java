/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package qualityFeatures.ZouNew;

import java.util.ArrayList;
import mslib.peak.Peak;
import mslib.spectrum.MassSpectrum;

/**
 *
 * @author aybuge
 * 
 * 
 
* 
* 
* Supportive ions. These features measure how likely one ion in the spectrum S is a supportive ion. This
paper considers two kinds of supportive ions a-ions and z- ions. Define
F14 = ∑{W(x, y)|dif1(m(x), m(y)) ≈ MCO or MNH} (15)
where MCO and MNH are the masses of a CO group and an NH group, respectively. The feature F14 measures the pres- ence of peak pairs of singly charged ions with a difference
of a CO or NH group in the spectrum S
* 
* Source : Zou, An-Min Wu, Fang-Xiang Ding, Jia-Rui Poirier, Guy G, 
Quality assessment of tandem mass spectra using support vector machine (SVM), BMC bioinformatics, 2009, DOI : 10.1186/1471-2105-10-S1-S49
 */
public class SupportiveIonsFromSinglyCharged extends Features {
    public SupportiveIonsFromSinglyCharged (String name, MassSpectrum ms) {
        super(name, ms);
    }

    public double calcScore() {
        double counter = 0;
        DetermineChargeOfSpectrum defineCharge = new DetermineChargeOfSpectrum(ms.getSpectrum());
        defineCharge.determineChargeOfSpectrum();
        ArrayList<Peak> returnSinglyCharged = DetermineChargeOfSpectrum.returnSinglyCharged();
        int sizeOfSinglyChargedArray = returnSinglyCharged.size();
        for (int k =0 ; k <sizeOfSinglyChargedArray-1 ; k++){
            for (int i =1 ; i <sizeOfSinglyChargedArray ; i ++){
               double diff = returnSinglyCharged.get(k).getX() - returnSinglyCharged.get(i).getX();
//               if(diff == MassOfCO || diff == MassOfNH || diff == MassOfCO + 2 || diff==MassOfCO -2 || diff==MassOfNH +2 || diff== MassOfNH -2){
//                   counter++;
//               }
                 if(Math.abs(diff-(MassOfCO)) <0.5 || Math.abs(diff-(MassOfNH)) <0.5)
                     counter++;
            }
            
        }
        return counter;
    }
      
}
