/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package qualityFeatures.ZouNew;

import java.util.ArrayList;
import mslib.peak.Peak;
import mslib.spectrum.MassSpectrum;

/**
 *num_dominat_peaks: Number of peaks accounting for > 5 % of total intensity
 * 
 * Source : Kristian Flikka, LennartMartens, Joël Vandekerckhove, Kris Gevaert, and Ingvar Eidhammer , Improving the reliability and throughput of mass spectrometry-basedproteomics by spectrumquality filtering , Proteiomcs, 2006, DOI: 10.1002/pmic.200500309
 * @author Aybuge
 */
public class NumberOfDominatPeaks extends Features{
    ArrayList<Peak> spectrumList = new ArrayList<Peak>();

    public NumberOfDominatPeaks(String name, MassSpectrum ms) {
        super(name, ms);
        this.spectrumList = spectrumList;
    }
    
    @Override
    public double calcScore(){
        ArrayList<Peak> dominatPeaks = new ArrayList<Peak>();
        double totalIntensity = 0.0; 
        for (Peak p : spectrumList) {
            totalIntensity = p.getY();
            if(p.getY() > (totalIntensity / 20))
                dominatPeaks.add(p);
        }
        return (double) dominatPeaks.size();
    }
}
