/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package qualityFeatures.ZouNew;

import java.util.ArrayList;
import mslib.peak.Peak;
import mslib.spectrum.MassSpectrum;

/**
 *F3: The number of peaks with relative intensity >0.1, square root-transformed. In this study, the relative
 * intensity of each peak is defined as the peak's intensity divided by the intensity of the highest peak
 * Source : Zou, An-Min Wu, Fang-Xiang Ding, Jia-Rui Poirier, Guy G, Quality assessment of tandem mass spectra using support vector machine (SVM), BMC bioinformatics, 2009, DOI : 10.1186/1471-2105-10-S1-S49
 * @author aybuge
 */
public class NumOfPeakWithRelativeIntensityMoreThan01Sqrt extends Features{     // 
    ArrayList <Peak> spectrumList = new ArrayList<Peak> ();
    public NumOfPeakWithRelativeIntensityMoreThan01Sqrt(String name, MassSpectrum ms, ArrayList<Peak> spectrumList) {
        super(name, ms);
        this.spectrumList = spectrumList;
    }
    
    @Override
    public double calcScore(){
        ms.sort(Peak.byYdesc);
            Peak highlyIntensePeak = ms.getPeak(0);
            double counterF3 = 0;                                               
            for (Peak p : spectrumList) {
                double relativeIntensity = p.getY() / highlyIntensePeak.getY();
                if (relativeIntensity > 0.1) {
                    counterF3++;
                }
            }
      return Math.sqrt(counterF3);      
    }
}
