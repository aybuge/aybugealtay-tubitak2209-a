/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package qualityFeatures.ZouNew;

import java.util.ArrayList;
import mslib.peak.Peak;
import mslib.spectrum.MassSpectrum;

/**
 * Standart Deviation of Average Delta mass : First variance ins calculated and then std dev is calculted. Variance is calculated as: 1. substraction of each value from the mean 2. sum of power of subtrahend
 * Then std dev is calculated as squareroot of variance
 * Source : Kristian Flikka, LennartMartens, Joël Vandekerckhove, Kris Gevaert, and Ingvar Eidhammer , Improving the reliability and throughput of mass spectrometry-basedproteomics by spectrumquality filtering , Proteiomcs, 2006, DOI: 10.1002/pmic.200500309
 *
 * @author Aybuge
 */
public class StdDevOfAverageDeltaMass extends AverageDeltaMass {

    ArrayList<Peak> spectrumList = new ArrayList<Peak>();

    public StdDevOfAverageDeltaMass(String name, MassSpectrum ms, ArrayList<Peak> spectrumList) {
        super(name, ms, spectrumList);
    }

    @Override
    public double calcScore() {
        double variance = 0.0;
        for (int i = 0; i < spectrumList.size(); i++) {
            variance = ((Math.pow((spectrumList.get(i).getX() - super.calcScore()) , 2))/spectrumList.size());
        }
       return Math.sqrt(variance);
    }

}
