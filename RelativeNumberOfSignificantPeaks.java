/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package qualityFeatures.ZouNew;

import java.util.ArrayList;
import mslib.peak.Peak;
import mslib.spectrum.MassSpectrum;

/**
 * Relative number of significant peaks: "We defined the relative intensity of each peak as the peak’s intensity divided by the intensity ofthe highest peak. When specified, we only considered peaks with a relative intensity above a certain threshold;we used 0.1."
 * Source : Kristian Flikka, LennartMartens, Joël Vandekerckhove, Kris Gevaert, and Ingvar Eidhammer , Improving the reliability and throughput of mass spectrometry-basedproteomics by spectrum quality filtering , Proteiomcs, 2006, DOI: 10.1002/pmic.200500309
 * @author Aybuge
 */
public class RelativeNumberOfSignificantPeaks extends NumberofPeaksWithRelativeIntensityMoreThan01{
    ArrayList <Peak> spectrumList = new ArrayList<Peak> ();
    public RelativeNumberOfSignificantPeaks(String name, MassSpectrum ms,ArrayList<Peak> spectrumList){
        super(name,  ms, spectrumList);
        this.spectrumList = spectrumList;
    }
    
    @Override
    public double calcScore (){
        ms.sort(Peak.byYdesc);
            Peak highlyIntensePeak = ms.getPeak(0);
            double counterF3 = 0;                                               
            for (Peak p : spectrumList) {
                double relativeIntensity = p.getY() / highlyIntensePeak.getY();
                if (relativeIntensity > 0.1) {
                    counterF3++;
                }
            }
            return counterF3 / ms.getPrecursorMass();
    }
}
