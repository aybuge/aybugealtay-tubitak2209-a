/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package qualityFeatures.ZouNew;

import java.util.ArrayList;
import mslib.peak.Peak;
import mslib.spectrum.MassSpectrum;
import static qualityFeatures.ZouNew.Features.MassOfAmmonia;
import static qualityFeatures.ZouNew.Features.MassOfWater;

/**
 *
 * @author aybuge
 * 
 * 
 * Supportive ions. These features measure how likely one ion in the spectrum S is a supportive ion. This
paper considers two kinds of supportive ions a-ions and z- ions. Define
F15 = ∑{W(x, y)|dif1(m(x), m(y)) ≈ MCO/2 or MNH/2} (15)
where MCO and MNH are the masses of a CO group and an NH group, respectively. The feature F15 measures the pres- ence of peak pairs of doubly charged ions with a difference
of a CO or NH group in the spectrum S
 * The comparison implied by ≈ employs a tolerance, which was set to ± 0.5 Da for fragment ions and ± 2
Da for parent mass in this paper.
 * 
 * Source : Zou, An-Min Wu, Fang-Xiang Ding, Jia-Rui Poirier, Guy G, 
Quality assessment of tandem mass spectra using support vector machine (SVM), BMC bioinformatics, 2009, DOI : 10.1186/1471-2105-10-S1-S49
 */
public class SupportiveIonsFromDoublyCharged extends Features {
         public SupportiveIonsFromDoublyCharged (String name, MassSpectrum ms) {
        super(name, ms);
    }

    public double calcScore() {
        double counter = 0;
        DetermineChargeOfSpectrum defineCharge = new DetermineChargeOfSpectrum(ms.getSpectrum());
        defineCharge.determineChargeOfSpectrum();
        ArrayList<Peak> returnDoublyCharged = DetermineChargeOfSpectrum.returnDoublyCharged();
        int sizeOfDoublyChargedArray = returnDoublyCharged.size();
        
        for (int k =0 ; k <sizeOfDoublyChargedArray-1 ; k++){
            for (int i =1 ; i <sizeOfDoublyChargedArray ; i ++){
               double diff = returnDoublyCharged.get(k).getX() - returnDoublyCharged.get(i).getX();
//               if(diff == MassOfCO/2 || diff == MassOfNH/2 || diff == (MassOfCO/2) + 0.5| diff==(MassOfCO/2) -0.5 || diff==(MassOfNH/2) +0.5 || diff== (MassOfNH/2) -0.5 ){
//                   counter++;
//               }
                 if(Math.abs(diff-(MassOfCO/2)) <0.5 || Math.abs(diff-(MassOfNH/2)) <0.5)
                     counter++;
            }
            
        }
        return counter;
    }   
}
