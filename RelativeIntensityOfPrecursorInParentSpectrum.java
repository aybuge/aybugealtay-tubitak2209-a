/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package qualityFeatures.ZouNew;

import java.util.ArrayList;
import mslib.peak.Peak;
import mslib.spectrum.MassSpectrum;

/**
 * Relative Intensity of precursor in parent spectrum : We defined the relative
 * intensity of each peak as the peak’s intensity divided by the intensity ofthe
 * highest peak. When specified, we only considered peaks with a relative
 * intensity above a certain threshold;weused0.1.
 * 
 * Source : Kristian Flikka, LennartMartens, Joël Vandekerckhove, Kris Gevaert, and Ingvar Eidhammer , Improving the reliability and throughput of mass spectrometry-basedproteomics by spectrumquality filtering , Proteiomcs, 2006, DOI: 10.1002/pmic.200500309
 * @author Aybuge
 */
public class RelativeIntensityOfPrecursorInParentSpectrum extends Features {

    ArrayList<Peak> spectrumList = new ArrayList<Peak>();

    public RelativeIntensityOfPrecursorInParentSpectrum(String name, MassSpectrum ms, ArrayList<Peak> spectrumList) {
        super(name, ms);
        this.spectrumList = spectrumList;
    }

    @Override
    public double calcScore() {
        double relativeIntensity= 0.0;

        ms.sort(Peak.byYdesc);
        Peak highlyIntensePeak = ms.getPeak(0);
        for (Peak p : spectrumList) {
            relativeIntensity = ms.getPrecursorIntensity() / highlyIntensePeak.getY();
        }
        return relativeIntensity;
    }
}
