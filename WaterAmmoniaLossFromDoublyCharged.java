/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package qualityFeatures.ZouNew;

import java.util.ArrayList;
import mslib.peak.Peak;
import mslib.spectrum.MassSpectrum;
import static qualityFeatures.ZouNew.Features.MassOfAmmonia;
import static qualityFeatures.ZouNew.Features.MassOfH;
import static qualityFeatures.ZouNew.Features.MassOfWater;

/**
 *
 * @author aybuge
 * 
 * Water or ammonia losses. These features measure how likely one ion in the spectrum S is produced by losing a water or ammonia molecule from a b-ion or y-ion.
 * F12 = ∑{W(x, y)|dif1(m(x), m(y)) ≈ Mw/2 or Ma/2} 
 * the feature F12 measures the presence of peak pairs of doubly charged ions with a difference of a water or
    ammonia molecule in the spectrum S
 * The comparison implied by ≈ employs a tolerance, which was set to ± 0.5 Da for fragment ions and ± 2
Da for parent mass in this paper.
 * 
 * Source : Zou, An-Min Wu, Fang-Xiang Ding, Jia-Rui Poirier, Guy G, 
Quality assessment of tandem mass spectra using support vector machine (SVM), BMC bioinformatics, 2009, DOI : 10.1186/1471-2105-10-S1-S49
 */
public class WaterAmmoniaLossFromDoublyCharged extends Features{
        public WaterAmmoniaLossFromDoublyCharged (String name, MassSpectrum ms) {
        super(name, ms);
    }

    public double calcScore() {
        double counter = 0;
        DetermineChargeOfSpectrum defineCharge = new DetermineChargeOfSpectrum(ms.getSpectrum());
        defineCharge.determineChargeOfSpectrum();
        ArrayList<Peak> returnDoublyCharged = DetermineChargeOfSpectrum.returnDoublyCharged();
        int sizeOfDoublyChargedArray = returnDoublyCharged.size();
        for (int k =0 ; k <sizeOfDoublyChargedArray-1 ; k++){
            for (int i =1 ; i <sizeOfDoublyChargedArray ; i ++){
               double diff = returnDoublyCharged.get(k).getX() - returnDoublyCharged.get(i).getX();
//               if(diff == MassOfAmmonia/2 || diff == MassOfWater/2 || diff == (MassOfAmmonia/2) + 0.5| diff==(MassOfAmmonia/2) -0.5 || diff==(MassOfWater/2) +0.5 || diff== (MassOfWater/2) -0.5 ){
//                   counter++;
//               }
                 if(Math.abs(diff-(MassOfAmmonia/2)) <0.5 || Math.abs(diff-(MassOfWater/2)) <0.5)
                         counter++;
            }
            
        }
        return counter;
    }
    

}
