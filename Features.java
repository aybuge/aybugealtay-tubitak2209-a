/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package qualityFeatures.ZouNew;

/**
 *
 * @author aybuge
 */
import mslib.spectrum.MassSpectrum;

public abstract class Features {
    public static final double MassOfH = 1.1;
    public static final double MassOfAmmonia = 17.031;
    public static final double MassOfWater = 18.015;
    public static final double  MassOfCO= 28.0101; 
    public static final double MassOfNH = 15.01464;
    public String name= "";
    public String source = "";
    public MassSpectrum ms = null;
    
    public Features(String name, MassSpectrum ms) {
        this.name = name;
        this.ms = ms;
        
    }

    // This prevents from using an empty constructor.
   // private Features() {
    //}
    
    abstract public double calcScore();
    
}