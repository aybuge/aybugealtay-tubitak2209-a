/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package qualityFeatures.ZouNew;

import java.util.ArrayList;
import mslib.peak.Peak;
import mslib.spectrum.MassSpectrum;

/**
 *
 * @author aybuge
 * Water or ammonia losses. These features measure how likely one ion in the spectrum S is produced by losing a water or ammonia molecule from a b-ion or y-ion.
 * F11 = ∑{W(x, y)|dif1(m(x), m(y)) ≈ Mw or M} 
 * the feature F11 measures the presence of peak pairs of singly charged ions with a difference of a water or
    ammonia molecule in the spectrum S
 * The comparison implied by ≈ employs a tolerance, which was set to ± 0.5 Da for fragment ions and ± 2
Da for parent mass in this paper.
 * 
 * Source : Zou, An-Min Wu, Fang-Xiang Ding, Jia-Rui Poirier, Guy G, 
Quality assessment of tandem mass spectra using support vector machine (SVM), BMC bioinformatics, 2009, DOI : 10.1186/1471-2105-10-S1-S49 
 */
public class WaterAmmoniaLossFromSinglyCharged extends Features{
    public WaterAmmoniaLossFromSinglyCharged (String name, MassSpectrum ms) {
        super(name, ms);
    }

    public double calcScore() {
        double counter = 0;
        DetermineChargeOfSpectrum defineCharge = new DetermineChargeOfSpectrum(ms.getSpectrum());
        defineCharge.determineChargeOfSpectrum();
        ArrayList<Peak> returnSinglyCharged = DetermineChargeOfSpectrum.returnSinglyCharged();
        int sizeOfSinglyChargedArray = returnSinglyCharged.size();
        for (int k =0 ; k <sizeOfSinglyChargedArray-1 ; k++){
            for (int i =1 ; i <sizeOfSinglyChargedArray ; i ++){
               double diff = returnSinglyCharged.get(k).getX() - returnSinglyCharged.get(i).getX();
//               if(diff == MassOfAmmonia || diff == MassOfWater || diff == MassOfAmmonia + 2 || diff==MassOfAmmonia -2 || diff==MassOfWater +2 || diff== MassOfWater -2){
//                   counter++;
//               }
                 if(Math.abs(diff-(MassOfAmmonia)) <0.5 || Math.abs(diff-(MassOfWater)) <0.5)
                         counter++;
            }
            
        }
        return counter;
    }
    
}
