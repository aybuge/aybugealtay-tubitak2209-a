/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package qualityFeatures.ZouNew;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.util.ArrayList;
import java.util.HashMap;
import mslib.filehandling.spectrafile.SpectraFile;
import mslib.filehandling.spectrafile.SpectraFileFactory;
import mslib.peak.Peak;
import mslib.spectra.Spectra;
import mslib.spectrum.MassSpectrum;
import seqs.AminoAcids;

/**
 *
 * @author aybuge
 */
public class Main {

    public static void main(String[] args) throws Exception {

        HashMap<String, Double> AADistances = new HashMap<String, Double>();
        AADistances.put("A", 71.0788);
        AADistances.put("R", 156.1875);
        AADistances.put("N", 114.1038);

        AminoAcids aaas = new AminoAcids();

        File directory = new File("C:\\Users\\aybuge\\Documents\\NetBeansProjects\\msmsqualityFinal\\data\\YWCNDGK MH +1_120725152328.mgf");
        
        SpectraFile newSpectraFile = SpectraFileFactory.getSuitableProcessor(directory);
        Spectra newSpectra = newSpectraFile.getSpectra();
        ArrayList<Features> fList = new ArrayList<Features>();
        for (MassSpectrum ms : newSpectra) {
            ArrayList<Peak> spectrumList = ms.getSpectrum();

//            NumOfPeaks NumOfPeaks = new NumOfPeaks("NumOfPeaks", ms, spectrumList);
//            NumOfPeaks.calcScore();
//            fList.add(NumOfPeaks);
//            AverageRawIntensity AverageRawIntensity = new AverageRawIntensity("AverageRawIntensity", ms, spectrumList);
//            AverageRawIntensity.calcScore();
//            fList.add(AverageRawIntensity);
//            NumOfPeakWithRelativeIntensityMoreThan01Sqrt NumOfPeakWithRelativeIntensityMoreThan01Sqrt = new NumOfPeakWithRelativeIntensityMoreThan01Sqrt("NumOfPeakWithRelativeIntensityMoreThan01Sqrt", ms, spectrumList);
//            NumOfPeakWithRelativeIntensityMoreThan01Sqrt.calcScore();
//            fList.add(NumOfPeakWithRelativeIntensityMoreThan01Sqrt);
//            NumOfPeakWithRelativeIntensityMoreThan01Log NumOfPeakWithRelativeIntensityMoreThan01Log = new NumOfPeakWithRelativeIntensityMoreThan01Log("NumOfPeakWithRelativeIntensityMoreThan01Log", ms ,spectrumList);
//            NumOfPeakWithRelativeIntensityMoreThan01Log.calcScore();
//            DetermineChargeOfSpectrum DetermineChargeOfSpectrum = new DetermineChargeOfSpectrum(spectrumList);
//            DetermineChargeOfSpectrum.determineChargeOfSpectrum();
//            PeakPairsFromDoublyCharged NumOfPeakPairsFromDoublyCharged = new PeakPairsFromDoublyCharged("NumOfPeakPairsFromDoublyCharged", ms);
//            NumOfPeakPairsFromDoublyCharged.calcScore();
//            PeakPairsFromSinglyCharged NumOfPeakPairsFromSinglyCharged = new PeakPairsFromSinglyCharged("NumOfPeakPairsFromSinglyCharged", ms);
//            NumOfPeakPairsFromSinglyCharged.calcScore();
//            fList.add((NumOfPeakPairsFromSinglyCharged));
//            PeakPairsFromOneSingleOneDouble NumOfPeakPairsFromOneSingleOneDouble = new PeakPairsFromOneSingleOneDouble("NumOfPeakPairsFromOneSingleOneDouble", ms);
//            NumOfPeakPairsFromOneSingleOneDouble.calcScore();
//            ComplementsFromDoublyCharged complementsFromDoublyCharged = new ComplementsFromDoublyCharged("ComplementsFromDoublyCharged", ms);
//            complementsFromDoublyCharged.calcScore();
//            fList.add(complementsFromDoublyCharged);
//            ComplementsFromSinglyCharged complementsFromSinglyCharged = new ComplementsFromSinglyCharged("ComplementsFromSinglyCharged", ms);
//            complementsFromSinglyCharged.calcScore();
//            ComplementsFromOneSingleOneDouble complementsFromOneSingleOneDouble = new ComplementsFromOneSingleOneDouble("ComplementsFeomOneSingleOneDouble", ms);
//            complementsFromOneSingleOneDouble.calcScore();
//              AveragePeakDensity averagePeakDensity = new AveragePeakDensity("AveragePeakDensity",  ms,  spectrumList);
//              averagePeakDensity.calcScore();
//              fList.add(averagePeakDensity);
            RelativeNumberOfSignificantPeaks relativeNumberOfSignificantPeaks = new RelativeNumberOfSignificantPeaks("RelativeNumberOfSignificantPeaks", ms , spectrumList);
            relativeNumberOfSignificantPeaks.calcScore();
            fList.add(relativeNumberOfSignificantPeaks);

        }

        for (Features f : fList) {
            //System.out.println(f.name);
            String name = f.name;
            //System.out.println(f.calcScore());
            double score = f.calcScore();
            try{
                FileWriter fstream = new FileWriter("RelativeNumberOfSignificantPeaks_YWCNDGK MH +1_120725152328.txt", true);
                BufferedWriter out = new BufferedWriter(fstream);
                out.write("" + score);
                out.newLine();
                out.close();
            }
            catch (Exception e){
                }
            }

//            try {
//                PrintStream myconsole = new PrintStream(new File("output.txt"));
//                System.setOut(myconsole);
//                myconsole.print(f.name);
//                myconsole.print(f.calcScore());
//
//            }
//            catch(FileNotFoundException fx){
//                System.out.println(" fx");
//            }
        }
    }


