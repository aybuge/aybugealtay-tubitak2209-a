/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package qualityFeatures.ZouNew;

import java.util.ArrayList;
import mslib.peak.Peak;
import mslib.spectrum.MassSpectrum;

/**
 * Average delta mass : spectrumdaki tüm spectraların mass leri arasındaki
 * farkların ortalaması mı ? 
 * Source : Kristian Flikka, LennartMartens, Joël Vandekerckhove, Kris Gevaert, and Ingvar Eidhammer , Improving the reliability and throughput of mass spectrometry-basedproteomics by spectrumquality filtering , Proteiomcs, 2006, DOI: 10.1002/pmic.200500309
 *
 * @author Aybuge
 */
public class AverageDeltaMass extends Features {

    ArrayList<Peak> spectrumList = new ArrayList<Peak>();

    public AverageDeltaMass(String name, MassSpectrum ms, ArrayList<Peak> spectrumList) {
        super(name, ms);
        this.spectrumList = spectrumList;
    }
    
   
    @Override
    public double calcScore() {
        double SumOfDeltaMass = 0.0;
        for (int k = 0; k < spectrumList.size() - 1; k++) {
            for (int i = 0; i < spectrumList.size(); i++) {
            SumOfDeltaMass = spectrumList.get(k).getX() - spectrumList.get(i).getX();                
            }
        }
        return SumOfDeltaMass/ (double) spectrumList.size();
    }
}
