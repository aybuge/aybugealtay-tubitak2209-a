/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package qualityFeatures.ZouNew;

import java.util.ArrayList;
import mslib.peak.Peak;
import mslib.spectrum.MassSpectrum;
import static qualityFeatures.ZouNew.Features.MassOfH;
import seqs.AminoAcid;
import seqs.AminoAcids;

/**
 *
 * @author aybuge
 * 
 * Complements. These features measure how likely an N-terminus ion and a C-terminus ion in the spectrum
S are produced as the peptide fragments at the same pep- tide bond.
F8 measures the presence of complementary peak pairs of singly charged ions in the spectrum S
F8 = ∑ {W(x, y)|sum1(m(x), m(y))≈ Mp/2 + 2m(H)}
sum1= ((m(x), m(y)) = m(x)+m(y)
* The comparison implied by ≈ employs a tolerance, which was set to ± 0.5 Da for fragment ions and ± 2
Da for parent mass in this paper. 
Source : Zou, An-Min Wu, Fang-Xiang Ding, Jia-Rui Poirier, Guy G, 
Quality assessment of tandem mass spectra using support vector machine (SVM), BMC bioinformatics, 2009, DOI : 10.1186/1471-2105-10-S1-S49
 */
public class ComplementsFromSinglyCharged extends Features {

    public ComplementsFromSinglyCharged(String name, MassSpectrum ms) {
        super(name, ms);
    }
    @Override
    public double calcScore() {
        DetermineChargeOfSpectrum defineCharge = new DetermineChargeOfSpectrum(ms.getSpectrum());
        defineCharge.determineChargeOfSpectrum();
        ArrayList<Peak> returnSinglyCharged = DetermineChargeOfSpectrum.returnSinglyCharged();
        int sizeOfSinglyChargedArray = returnSinglyCharged.size();
        double counter = 0.0;
        for (int k = 0; k < sizeOfSinglyChargedArray - 1; k++) {
            for (int i = sizeOfSinglyChargedArray-1 ; i > 0 ; i--) {
                double sum = returnSinglyCharged.get(k).getX() + returnSinglyCharged.get(i).getX();
                    if (Math.abs(sum - ms.getPrecursorMass()) <=2)  
                    counter++; 
            }
        }
        return counter;
    }
}
