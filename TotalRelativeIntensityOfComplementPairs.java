/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package qualityFeatures.ZouNew;

import java.util.ArrayList;
import mslib.peak.Peak;
import mslib.spectrum.MassSpectrum;

/**
 * rel_int_by_compl_to_prec : Total relative intensity of complment pairs
 *
 * Source : Kristian Flikka, LennartMartens, Joël Vandekerckhove, Kris Gevaert,
 * and Ingvar Eidhammer , Improving the reliability and throughput of mass
 * spectrometry-basedproteomics by spectrumquality filtering , Proteiomcs, 2006,
 * DOI: 10.1002/pmic.200500309
 *
 * @author Aybuge
 */
public class TotalRelativeIntensityOfComplementPairs extends Features {

    ArrayList<Peak> spectrumList = new ArrayList<Peak>();

    public TotalRelativeIntensityOfComplementPairs(String name, MassSpectrum ms, ArrayList<Peak> spectrumList) {
        super(name, ms);
        this.spectrumList = spectrumList;
    }

    @Override
    public double calcScore() {
        ms.sort(Peak.byYdesc);
        Peak highlyIntensePeak = ms.getPeak(0);
        double totalRelativeIntensity = 0.0;
        for (int k = 0; k < spectrumList.size() - 1; k++) {
            for (int i = spectrumList.size() - 1; i > 0; i--) {
                double sum = spectrumList.get(k).getX() + spectrumList.get(i).getX();
                if (sum == ms.getPrecursorMass() || sum == (ms.getPrecursorMass() / 2)) { // we have to know whethe rit is singly of doubly???
                    for (Peak p : spectrumList) {
                        double relativeIntensity = p.getY() / highlyIntensePeak.getY();
                        if (relativeIntensity > 0.1) {
                            totalRelativeIntensity += relativeIntensity;
                        }
                    }
                }
            }
        }
        return totalRelativeIntensity;
    }
}
