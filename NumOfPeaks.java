/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package qualityFeatures.ZouNew;

import java.util.ArrayList;
import mslib.peak.Peak;
import mslib.spectrum.MassSpectrum;

/**
 *F1: The number of peaks in the spectrum, square root- transformed.
 * Source : Zou, An-Min Wu, Fang-Xiang Ding, Jia-Rui Poirier, Guy G, Quality assessment of tandem mass spectra using support vector machine (SVM), BMC bioinformatics, 2009, DOI : 10.1186/1471-2105-10-S1-S49
 * 
 * @author aybuge
 */
public class NumOfPeaks extends Features {                                      // 

   ArrayList <Peak> spectrumList = new ArrayList<Peak> ();
   protected int spectrumSize =0;
   
    public NumOfPeaks(String name, MassSpectrum ms, ArrayList<Peak> spectrumList) {
        super(name, ms);
        this.spectrumList = spectrumList;
        this.spectrumSize = spectrumList.size();
        
    }
    
//    public double calcNumOfPeaks(ArrayList<Peak> spectrumList){
//        double NumOfPeaks = (double) Math.sqrt(spectrumSize);         
//        return NumOfPeaks;
//    }
    @Override
    public double calcScore(){                      
        double NumOfPeaks = (double) Math.sqrt(spectrumSize);         
        return NumOfPeaks;
    }
            
    
}
